package softserve.academy.us303;

import org.testng.annotations.*;
import softserve.academy.actions.AdminPanelActions;
import softserve.academy.actions.GroupsPageActions;
import softserve.academy.actions.LoginPageActions;
import softserve.academy.actions.StudentsPageActions;
import softserve.academy.modal_windows.MWSingleStudentActions;
import softserve.academy.models.Group;
import softserve.academy.models.Student;

import static softserve.academy.models.GroupDataProvider.us107AdminLviv;
import static softserve.academy.models.StudentDataProvider.student303;
import static softserve.academy.models.UserDataProvider.admin107;
import static softserve.academy.models.Utils.BASE_URL;
import static softserve.academy.models.Utils.getDriver;
import static softserve.academy.models.Utils.setDriver;

public class Story303AsCoordinatorEditStudent {
    Group group = us107AdminLviv;
    Student student = student303;
    String existingGroup = "Lv-023-UX";

    @BeforeClass(description = "Preconditions for test Coordinator can edit " +
            "incoming score field")
    public void setUp() {

        setDriver()
                .manage()
                .window()
                .maximize();

        new LoginPageActions()
                .openLoginPage()
                .login(admin107)
                .waitPageLoaded()
                .goToAdminPanel()
                .openGroupTab()
                .openCreateModal()
                .fillAllGroupFields(group)
                .submitCreateEdit()
                .escapeToGroupPage().waitGroupsPageLoad()
                .openStudentsPage()
                .selectGroup(existingGroup)
                .editStudentList()
                .clickCreateStudent()
                .setAllStudentRequiredFields(student)
                .clickIconExit();
    }

    @BeforeMethod
    public void openStudentsPage(){
        getDriver().navigate().to(BASE_URL);
    }

    @Test(description = "Coordinator can edit Income mark of students")
    public void coordinatorCanEditIncomingScoreField() {
        new GroupsPageActions()
                .openStudentsPage()
                .selectGroup(existingGroup)
                .editStudentList()
                .clickButtonEditStudent(student)
                .setCustomMarkIncomingScore(111)
                .clickConfirm()
                .clickButtonEditStudent(student)
                .verifyIncomingTestValueIsEequals(111)
                .assertAll();
    }

    @Test(description = "Coordinator can edit Approved By")
    public void coordinatorCanEditApprovedByField() {
        new GroupsPageActions()
                .openStudentsPage()
                .selectGroup(existingGroup)
                .editStudentList()
                .clickButtonEditStudent(student)
                .setCustomApprovedBy("CusomApr")
                .clickConfirm()
                .clickButtonEditStudent(student)
                .verifyIncomingTestValueIsEequals("CusomApr")
                .assertAll();
    }



    @AfterMethod
    public void closeModalWindows(){
        new MWSingleStudentActions().clickClose().clickIconExit();
    }

    @AfterClass(description = "Remove test student and group")
    public void tearDown() {
        getDriver().navigate().to(BASE_URL);

        new GroupsPageActions()
                .openStudentsPage()
                .selectGroup(existingGroup)
                .editStudentList()
                .clickDeleteStudent(student)
                .confirm();
        new AdminPanelActions()
                .openAdminPanel()
                .openGroupTab()
                .deleteRowByName(group.getName());

        getDriver().quit();
    }
}
