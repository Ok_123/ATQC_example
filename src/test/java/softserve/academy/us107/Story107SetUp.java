package softserve.academy.us107;

import org.testng.annotations.*;
import softserve.academy.actions.AdminPanelActions;
import softserve.academy.actions.LoginPageActions;
import softserve.academy.models.Group;
import softserve.academy.models.GroupDataProvider;

import static softserve.academy.models.UserDataProvider.admin107;
import static softserve.academy.models.Utils.getDriver;
import static softserve.academy.models.Utils.setDriver;

public class Story107SetUp {
    AdminPanelActions adminPage;

    @Test(priority = 0, dataProvider = "GroupDataProvider")
    public void SetUp(Group group) {
       adminPage
                .openCreateModal()
                .fillAllGroupFields(group)
                .submitCreateEdit();
    }

    @BeforeClass
    public void login() {
        setDriver().manage().window().maximize();
        new LoginPageActions()
                .openLoginPage()
                .waitLogInPageLoad()
                .login(admin107)
                .waitPageLoaded();
        adminPage = new AdminPanelActions()
                .openAdminPanel()
                .openGroupTab();
    }

    @AfterClass
    public void quit() {
        getDriver().quit();
    }

    @DataProvider(name = "GroupDataProvider")
    public Object[][] groupCreateData() {
        return new Object[][]{
                {GroupDataProvider.us107TeacherRivne},
                {GroupDataProvider.us107CoordinatorKyiv},
                {GroupDataProvider.us170TeacherLviv},
                {GroupDataProvider.us107AdminLviv},
                {GroupDataProvider.us107AdminKyiv},
                {GroupDataProvider.us107CoordinatorRivne}
        };
    }
}
