package softserve.academy.us101;


import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.*;
import softserve.academy.actions.LoginPageActions;
import softserve.academy.models.Location;
import softserve.academy.models.User;
import softserve.academy.pages.TopMenu;

import java.util.logging.Level;
import java.util.logging.Logger;

import static softserve.academy.models.Utils.getDriver;
import static softserve.academy.models.Utils.setDriver;

public class BaseTest {


	protected static final Logger LOGGER = Logger.getLogger(softserve.academy.BaseTest.class.getName());
	protected Level level = Level.INFO;

	@Parameters({"login", "password"})
	@BeforeTest(groups = {"schedulePage"}, description = "Preconditions for schedulePage test")
	public void setUpForSchedulePageTest(String login, String password) {
		LOGGER.log(level, "schedulePage group, open browser and login as teacher, select location ");

		setDriver()
				.manage()
				.window()
				.maximize();


		new LoginPageActions()
				.openLoginPage()
				.waitLogInPageLoad()
				.login(new User(login, password));

		new TopMenu()
				.openSelectLocationWindow()
				.clearChecks()
				.selectLocation(Location.DNIPRO)
				.save();
	}

	@Parameters({"login", "password"})
	@BeforeTest(groups = {"schedulePage2"}, description = "Preconditions for schedulePage2 test")
	public void setUpForSchedulePage2Test(String login, String password) {
		LOGGER.log(level, "schedulePage2 group, open browser and login as teacher... ");

		setDriver()
				.manage()
				.window()
				.maximize();


		new LoginPageActions()
				.openLoginPage()
				.waitLogInPageLoad()
				.login(new User(login, password))
				.waitPageLoaded();


	}

	@AfterMethod(groups = {"schedulePage"}, description = "Preconditions for schedulePage test")
	public void updatePage() {
		LOGGER.log(level, "schedulePage group, update page... ");
		getDriver().get("http://146.148.17.49/");
	}

	@BeforeClass(groups = {"schedulePage2"}, description = "Preconditions for ordinary test")
	public void updatePageBefore() {
		LOGGER.log(level, "schedulePage2 group, update page... ");
		getDriver().get("http://146.148.17.49/");
		new TopMenu()
				.openSelectLocationWindow()
				.clearChecks()
				.selectLocation(Location.DNIPRO)
				.save();

		new TopMenu()
				.goToSchedulePage()
				.waitPageLoaded();
	}


	@AfterTest(groups = {"schedulePage", "schedulePage2"}, description = "Preconditions for schedulePage2, schedulePage" +
			" test")
	public void close() {
		LOGGER.log(level, "ordinary group,logout and close browser... ");
		getDriver().get("http://146.148.17.49/logout");
		getDriver().quit();
	}


	@BeforeClass(groups = {"loginPage"}, description = "Preconditions for login test")
	public void setUpforLogin() {
		LOGGER.log(level, "login group, open browser... ");

		setDriver()
				.manage()
				.window()
				.maximize();


	}

	@AfterMethod(groups = {"loginPage"}, description = "")
	public void logout() {
		LOGGER.log(level, "login group,logout ...");
		getDriver().get("http://146.148.17.49/logout");

	}

	@AfterClass(groups = {"loginPage"}, description = "")
	public void closeBrowser() {
		LOGGER.log(level, "login group, close browser ...");
		getDriver().quit();
	}


}

