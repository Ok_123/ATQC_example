package softserve.academy.AdminTests;

import org.testng.annotations.*;
import softserve.academy.actions.AdminPanelActions;
import softserve.academy.actions.LoginPageActions;
import softserve.academy.models.User;
import softserve.academy.models.UserDataProvider;

import static softserve.academy.models.Utils.getDriver;
import static softserve.academy.models.Utils.setDriver;

@Test(groups ={"admin", "feedback"})
public class AdminCreateUserAndLogInGroupPageTest{

    @Test(dataProvider = "UserDataProvider")
    public void createLoginDeleteLoginUsers(User user) {
        new AdminPanelActions()
                .openAdminPanel()
                .openUserTab()
                .openCreateModal()
                .fillAllUserFields(user)
                .submitCreateEdit()
                .logOut()
                .waitLogInPageLoad()
                .login(user.getLogin(), user.getPassword())
                .waitPageLoaded()
                .verifyURLContains(user.getLocation())
                .verifyUserPhoto(user)
                .goToAdminPanel()
                .deleteRowByName(user.getLogin())
                .verifyRowWithNameNotPresentInTab(user.getLogin(), "users")
                .logOut()
                .incorrectLogin(user)
                .verifyHardIncorrectDataMessageIsDisplayed();
    }


    @DataProvider(name="UserDataProvider")
    public Object[][] userCreateData() {
        return new Object[][]{
                {UserDataProvider.userToCreate1},
                {UserDataProvider.userToCreate2},
                {UserDataProvider.userToCreate1LoginFromCapital}
        };
    }

    User userLogIn = new User("alena", "1234");
    @BeforeTest(groups = {"admin", "feedback"})
    public void startDriver() {
        setDriver()
                .manage()
                .window()
                .maximize();

    }
    @BeforeMethod(groups = {"admin", "feedback"})
    public void logIn(){
        new LoginPageActions()
                .openLoginPage()
                .waitLogInPageLoad()
                .login(new User(userLogIn.getLogin(), userLogIn.getPassword()))
                .waitPageLoaded();
    }

    @AfterMethod(alwaysRun = true, groups = {"admin", "feedback"})
    public void logOut() {

    }
    @AfterTest(alwaysRun = true, groups = {"admin", "feedback"})
    public void quit(){
        getDriver().quit();
    }

}
