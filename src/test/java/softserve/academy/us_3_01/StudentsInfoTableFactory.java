package softserve.academy.us_3_01;

import org.testng.annotations.Factory;

public class StudentsInfoTableFactory {

    @Factory
    public Object[] factoryMethod() {
        return new Object[]{
                new StudentsInfoTableTest("sasha", "1234"),
                new StudentsInfoTableTest("dmytro", "1234"),
                new StudentsInfoTableTest("admin", "1234")
        };
    }

}
