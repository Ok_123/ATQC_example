package softserve.academy.us_3_01;

import org.testng.annotations.Test;
import softserve.academy.BaseTestK;
import softserve.academy.models.Location;
import softserve.academy.pages.TopMenu;

public class StudentsInfoTableTest extends BaseTestK {
    private static final String WORKING_GROUP = "Dp-149 TAQC";

    StudentsInfoTableTest(String login, String password) {
        super(login, password);
    }

    @Test
    void testStudentsTableHeadings() {
        new TopMenu()
                .openSelectLocationWindow()
                .clearChecks()
                .selectLocation(Location.DNIPRO)
                .save()
                .selectGroup(WORKING_GROUP)
                .openStudentsTab()
                .verifyStudentsInfoTable();
    }

    @Test
    void testTableRecordChangesColorAfterHoveringTheMouse() {
        new TopMenu()
                .openSelectLocationWindow()
                .clearChecks()
                .selectLocation(Location.DNIPRO)
                .save()
                .selectGroup(WORKING_GROUP)
                .openStudentsTab()
                .verifyTableRecordChangesColorWhenHovered();
    }

    @Test
    void testSortByName() {
        new TopMenu()
                .openSelectLocationWindow()
                .clearChecks()
                .selectLocation(Location.DNIPRO)
                .save()
                .selectGroup(WORKING_GROUP)
                .openStudentsTab()
                .sortByName()
                .verifyStudentsInfoTableIsSortedByName();
    }

    @Test
    void testSortByEnglishLevel() {
        new TopMenu()
                .openSelectLocationWindow()
                .clearChecks()
                .selectLocation(Location.DNIPRO)
                .save()
                .selectGroup(WORKING_GROUP)
                .openStudentsTab()
                .sortByEnglishLevel()
                .verifyStudentsInfoTableIsSortedByEnglishLevel();
    }
}
