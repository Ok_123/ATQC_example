package softserve.academy.actions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import softserve.academy.models.Group;
import softserve.academy.models.Student;
import softserve.academy.models.User;
import softserve.academy.pages.adminPanel.ModalCreateEditGroup;
import softserve.academy.pages.adminPanel.ModalCreateEditStudent;
import softserve.academy.pages.adminPanel.ModalCreateEditUser;

import java.util.concurrent.TimeUnit;

import static softserve.academy.models.Utils.BASE_URL;
import static softserve.academy.models.Utils.getDriver;

public class AdminModalActions extends BaseActions{
    ModalCreateEditUser modalUser;
    ModalCreateEditGroup modalGroup;
    ModalCreateEditStudent modalStudent;

    public AdminModalActions (){};

    public AdminModalActions (String ID){
        switch(ID){
            case "users":  modalUser = new ModalCreateEditUser();
                break;
            case "groups": modalGroup =new ModalCreateEditGroup();
                break;
            case "students": modalStudent=new ModalCreateEditStudent();
        }
    }

    //******************************************************************************************************************
    // To Create NEW Entity:
    public AdminModalActions fillAllUserFields(User user){
        fillField(modalUser.fieldFirstName, user.getFirstName());
        fillField(modalUser.fieldLastName, user.getLastName());
        fillOptionField(modalUser.fieldRole, user.getRole().toString());
        fillOptionField(modalUser.fieldLocation, user.getLocation().toString());
        fillField(modalUser.fieldPhoto, user.getPhotopath());
        fillField(modalUser.fieldLogin, user.getLogin());
        fillField(modalUser.fieldPassword, user.getPassword());
        return this;
    }

    public AdminModalActions fillAllGroupFields(Group group){
        fillField(modalGroup.fieldName, group.getName());
        fillOptionField(modalGroup.fieldLocation, group.getLocation());
        if (group.isBudgetOwner()==true) {
            modalGroup.boxBudgetOwner.click();
        }
        fillOptionField(modalGroup.fieldDirection, group.getDirection());
        fillField(modalGroup.fieldStartDate, group.getStartDate());
        fillField(modalGroup.fieldFinishDate, group.getFinishDate());
        fillField(modalGroup.fieldTeachers, group.getTeacher());
        fillField(modalGroup.fieldExperts, group.getExpert());
        fillOptionField(modalGroup.fieldStage, group.getStage());
        return this;
    }

    public AdminModalActions fillAllStudentFields(Student student){
        fillField(modalStudent.fieldGroupId, student.getGroupID());
        fillField(modalStudent.fieldName, student.getName());
        fillField(modalStudent.fieldLastName, student.getLastName());
        fillOptionField(modalStudent.fieldEnglishLevel, student.getEnglishLevel1());
        fillField(modalStudent.fieldCvUrl, student.getCv());
        fillField(modalStudent.fieldImageUrl, student.getPhoto());
        fillField(modalStudent.fieldEntryScore, student.getEntryScore1());
        fillField(modalStudent.fieldApprovedBy, student.getApprovedBy1());
        return this;
    }


    //********************************************************************************************************
    // To Edit existing Entity:
    public AdminModalActions editUserField(String fieldName, String textToEnter){
        WebElement field=getDriver().findElement(By.name(fieldName));
        if (!fieldName.equals("role") && !fieldName.equals("location")){
            fillField(field, textToEnter);
        } else fillOptionField(field,textToEnter);
        return this;
    }

    public AdminModalActions editGroupField(String fieldName, String textToEnter) {
        WebElement field = getDriver().findElement(By.name(fieldName));
        if (fieldName.equals("budgetOwner")) {
            WebElement checkBox = field;
            if ((checkBox.getAttribute("checked") == null && textToEnter.equals("true"))
                    || (checkBox.getAttribute("checked").equals("true") && textToEnter.equals("false"))) {
                waitClickable(checkBox).click();
            }
        } else
        if (fieldName.equals("location") || fieldName.equals("direction")
                || fieldName.equals("stage")) {
            fillOptionField(field, textToEnter);
        } else fillField(field, textToEnter);
        return this;
    }

    public AdminModalActions editStudentField(String fieldName, String textToEnter){
        WebElement field=getDriver().findElement(By.name(fieldName));
        if (!fieldName.equals("role") && !fieldName.equals("location")){
            fillField(field, textToEnter);
        } else fillOptionField(field,textToEnter);
        return this;
    }

    public AdminPanelActions submitCreateEdit(){
        WebElement submit=getDriver().findElement(By.xpath("//*[contains(@class,'submit')]"));
        submit.click();
       wait.until(ExpectedConditions.invisibilityOf(submit));
     return new AdminPanelActions();
    }

    public AdminPanelActions closeCreateEdit(){
        WebElement close=getDriver().findElement(By.xpath("//*[contains(@class,'default')]"));
        waitVisible(close).click();
        wait.until(ExpectedConditions.invisibilityOf(close));
        return new AdminPanelActions();
    }
    //******************************************************************************************************************
    // Assertions


    public AdminModalActions verifyOpened() {
        boolean isOpened = false;

        if (modalUser != null) {
            isOpened = true;
        } else if (modalGroup != null) {
            isOpened = true;
        } else if (modalStudent != null) {
            isOpened = true;
        }
        Assert.assertTrue(isOpened);

        return this;
    }

    //************************************************************************************************************

    //UTILS


    public AdminModalActions pause(int i) {
        try {
            TimeUnit.SECONDS.sleep(i);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return this;
    }
}
