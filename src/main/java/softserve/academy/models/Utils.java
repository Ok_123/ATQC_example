package softserve.academy.models;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import softserve.academy.pages.BasePage;

public class Utils {

    public final static String BASE_URL = "http://146.148.17.49/";
    public final static String LOGOUT_URL = "http://146.148.17.49/logout";
    private final static String WIN_CHROMEDRV = "/chromedriver.exe";
    private static WebDriver driver;
    private final static String LINUX_CHROMEDRV = "/chromedriver";
    public static String ADMIN_POSTFIX = "admin";

    static {
        System.setProperty("webdriver.chrome.driver",
                BasePage.class.getResource(
                        System.getProperty("os.name").equals("Linux") ? LINUX_CHROMEDRV : WIN_CHROMEDRV).getPath());
    }

    public static WebDriver setDriver() {
        driver = new ChromeDriver();
        return driver;
    }

    public static WebDriver getDriver() {
        return driver;
    }

}