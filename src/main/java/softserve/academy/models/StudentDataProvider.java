package softserve.academy.models;

public class StudentDataProvider {
    public static final Student student303 = new Student("Name", "LastName", 20, 2);


    //For Admin Panel Tests**************************************************************************************
    public static final Student student1= new Student("DP-700","Grygoriy", "Gandja",
            EnglishLevel.INTERMEDIATE_LOW.getValue(), "3", Expert.RYBAKOV.getValue(), "", "");
    public static final Student student2= new Student("DP-700","Nelly", "Ledneva",
            EnglishLevel.INTERMEDIATE.getValue(), "3", Expert.KARASIK.getValue(), "", "");
    public static final Student studentBeforeEdit= new Student("DP-702","Genna", "Lyapishev",
            EnglishLevel.UPPER_INTERMEDIATE.getValue(), "3", Expert.I_KOHUT.getValue(), "", "");
    public static final Student studentAfterEdit= new Student("DP-802","Lena", "Lyapisheva",
            EnglishLevel.INTERMEDIATE.getValue(), "4", Expert.RYBAKOV.getValue(), "", "");
}
