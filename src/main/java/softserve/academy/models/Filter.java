package softserve.academy.models;

import org.openqa.selenium.By;

public enum Filter {

	FINISHED( By.xpath ("//label[@for='endedGroups']")),
	CURRENT( By.xpath ("//label[@for='currentGroups']")),
	PLANNED( By.xpath ( "//label[@for='futureGroups']"));


	private By value;


	Filter(final By xpath) {
		this.value = xpath;
	}

	public By getValue() {
		return value;
	}
}
